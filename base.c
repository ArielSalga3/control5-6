#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "estudiantes.h"

float desvStd(estudiante curso[])
{
	int i;
	float mediaAritmetica = 0.0, varianza = 0.0, desviacionEstandar;
	for (i = 0; i < 24; i++)
	{
		mediaAritmetica += curso[i].prom; 			//Se obtiene la media aritmetica sumando todos los promedios
	}												//y dividiendolo en la cantidad de estudiantes
	mediaAritmetica = (mediaAritmetica)/24;
	for (i = 0; i < 24; i++)
	{
		varianza += pow(curso[i].prom - mediaAritmetica, 2);		//Se obtiene la varianza restando cada uno de los promedios
	}																//con la media aritmetica y sumando cada una de las diferencias
	desviacionEstandar = sqrt(varianza/23);
	return desviacionEstandar;				//La desviacion estandar es la varianza al cuadrado de todos los promedios dividido en la muestra -1
}

float menor(estudiante curso[])
{
	int i;
	float menorProm = curso[0].prom;			//Se usa al primer estudiante como referente para comenzar a comparar con el resto
	for (i = 1; i < 24; i++)
	{
		if (menorProm > curso[i].prom)			//Si el siguiente estudiante tiene un promedio mas bajo que el denominado "promedio mas bajo"
		{										//este ocupara su lugar
			menorProm = curso[i].prom;
		}
	}
	return menorProm;
}

float mayor(estudiante curso[])					
{
	int i;
	float mayorProm = curso[0].prom;			//Se usa al primer estudiante como referente para comenzar a comparar con el resto
	for (i = 1; i < 24; i++)					
	{
		if (mayorProm < curso[i].prom)			//Si el siguiente estudiante tiene un promedio mas alto que el denominado "promedio mas alto"
		{										//este ocupara su lugar
			mayorProm = curso[i].prom;
		}
	}
	return mayorProm;
}

void registroCurso(estudiante curso[])
{
	int i;
	float promProy, promCont;
	for (i = 0; i < 24; i++)
	{
		printf("\nALUMNO: %s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		printf("Ingrese las notas de los proyectos(3): \n");
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3);			//Se hace un recorrido con la totalidad de estudiantes para poder ingresar ñas notas de los proyectos y controles
		printf("Ahora ingrese las notas de los controles(6): \n");
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, &curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, &curso[i].asig_1.cont6);
		promProy = (curso[i].asig_1.proy1 + curso[i].asig_1.proy2 + curso[i].asig_1.proy3)/3;																				//Se obtiene un promedio de las notas de los proyectos
		promCont = (curso[i].asig_1.cont1 + curso[i].asig_1.cont2 + curso[i].asig_1.cont3 + curso[i].asig_1.cont4 + curso[i].asig_1.cont5 + curso[i].asig_1.cont6)/6;		//Se obtiene un promedio de las notas de los controles
		curso[i].prom = (promProy + promCont)/2;			//El promedio del estudiante sera el promedio entre el promedio de los proyectos y el promedio de los controles
	}
	//printf("TEST"); 
}

void clasificarEstudiantes(char path[], estudiante curso[])
{
	int i;
	FILE *reprobados;		//Se crean archivos con sus respectivos nombres
	FILE *aprobados;
	reprobados = fopen("estudiantesReprobados.txt", "w");		//Se abren en modo escritura para poder empezar a escribir sobre ellos
	aprobados = fopen("estudiantesAprobados.txt", "w");
	for (i = 0; i < 24; i++)
	{
		if (curso[i].prom < 40)
		{
			fprintf(reprobados, "%s %s %s\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);			//Se hace un recorrido con todos los estudiantes y poder dividirlos entre aprobados y reprobados
		}																										//Si su notas es menor a cuatro se registrara su nombre dentro del archivo alumnosReprobados, de lo 
		else																									//Contrario se registrara su nombre en el archivo alumnosAprobados
		{
			fprintf(aprobados, "%s %s %s\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
	}
	fclose(reprobados);			//Se cierran ambos archivos
	fclose(aprobados);
	//printf("TEST"); 
}

void metricasEstudiantes(estudiante curso[])
{
	printf("\nLa desviacion estandar del curso es %.2f\n", desvStd(curso));			//Se llaman a las funciones que possen las metricas y se imprimen en pantalla los valores que estas obtienen
	printf("El peor promedio del curso es %.2f\n", menor(curso));
	printf("El mejor promedio del curso es %.2f\n", mayor(curso));
	//printf("TEST"); 
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes");
		printf( "\n   2. Ingresar notas");
		printf( "\n   3. Mostrar Promedios");
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes");
		printf( "\n   6. Salir.");
		printf( "\n\n   Introduzca opción (1-6): ");
		scanf( "%d", &opcion);
        switch (opcion){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;

            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while (opcion != 6);
}

int main()
{
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}